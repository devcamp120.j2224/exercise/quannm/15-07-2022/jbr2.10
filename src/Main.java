public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");

        Author author1 = new Author("Quan", "acongh@gmail.com", 'M');
        Author author2 = new Author("Boi", "boiboi@gmail.com", 'F');
        System.out.println(author1 + "," + author2);

        Book book1 = new Book("English", author1, 200);
        Book book2 = new Book("Math", author2, 300, 2);
        System.out.println(book1 + "," + book2);
    }
}